(function(){
  const button = document.getElementById('scrollDownButton');
  const aboutEmma = document.getElementById('aboutEmma');
  const aboutEmmaYOffset = aboutEmma.offsetTop;
  // console.dir(aboutEmma);
  // console.dir(window);

  // document.addEventListener('scroll', function(eve) {
  //   // console.log(aboutEmma.offsetTop);
  // });

  button.addEventListener('click', function(eve) {
    scrollToElement(aboutEmma);
  });

  function scrollToElement(element) {
    element.scrollIntoView({ behavior: 'smooth'});

  }
}());
# 01 Build a Tribute Page

* Objective: Build a [CodePen.io](https://codepen.io/) app that is functionally similar to this: https://codepen.io/freeCodeCamp/full/zNqgVx.

* Fulfill the below [user stories](https://en.wikipedia.org/wiki/User_story) and get all of the tests to pass. Give it your own personal style.

* You can use HTML, JavaScript, and CSS to complete this project. Plain CSS is recommended because that is what the lessons have covered so far and you should get some practice with plain CSS. You can use Bootstrap or SASS if you choose. Additional technologies (just for example jQuery, React, Angular, or Vue) are not recommended for this project, and using them is at your own risk. Other projects will give you a chance to work with different technology stacks like React. We will accept and try to fix all issue reports that use the suggested technology stack for this project. Happy coding!

* _User Story #1_: My tribute page should have an element with a corresponding `id="main"`, which contains all other elements.

* _User Story #2_: I should see an element with a corresponding `id="title"`, which contains a string (i.e. text) that describes the subject of the tribute page (e.g. "Dr. Norman Borlaug").

* _User Story #3_: I should see a `div` element with a corresponding `id="img-div"`.

* _User Story #4_: Within the `img-div` element, I should see an `img` element with a corresponding `id="image"`.

* _User Story #5_: Within the `img-div` element, I should see an element with a corresponding `id="img-caption"` that contains textual content describing the image shown in `img-div`.

* _User Story #6_: I should see an element with a corresponding `id="tribute-info"`, which contains textual content describing the subject of the tribute page.

* _User Story #7_: I should see an `a` element with a corresponding `id="tribute-link"`, which links to an outside site that contains additional information about the subject of the tribute page. HINT: You must give your element an attribute of `target` and set it to `_blank` in order for your link to open in a new tab (i.e. `target="_blank"`).

* _User Story #8_: The `img` element should responsively resize, relative to the width of its parent element, without exceeding its original size.

* _User Story #9_: The `img` element should be centered within its parent element.

* You can build your project by forking [this CodePen pen](http://codepen.io/freeCodeCamp/pen/MJjpwO). Or you can use this CDN link to run the tests in any environment you like: https://cdn.freecodecamp.org/testable-projects-fcc/v1/bundle.js.

* Once you're done, submit the URL to your working project with all its tests passing.

* Remember to use the [Read-Search-Ask](https://forum.freecodecamp.org/t/how-to-get-help-when-you-are-stuck/19514) method if you get stuck.